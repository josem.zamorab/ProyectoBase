package es.upv.master.proyectobase;

import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;


public class Procesado {

    private Mat salidaintensidad;
    private Mat salidatrlocal;
    private Mat salidabinarizacion;
    private Mat salidasegmentacion;
    private Mat salidaocr;
    private Mat pasoBajo;
    private Mat resPasoAltoNegativo;

    //Necesartio manejo de Histogramas
    private MatOfInt canales;
    private MatOfInt numero_bins;
    private MatOfFloat intervalo;
    private Mat hist;
    private List<Mat> imagenes;
    private float[] histograma;
    private List<Integer[]> profundidad;


    private Boolean pantallaPartida;
    private Salida mostrarSalida;
    private TipoIntensidad tipoIntensidad;
    private TipoOperadorLocal tipoOperadorLocal;
    private TipoBinarizacion tipoBinarizacion;
    private TipoSegmentacion tipoSegmentacion;
    private TipoReconocimiento tipoReconocimiento;

    private final static int ROJO = 101;
    private final static int VERDE = 102;
    private final static int AZUL = 103;
    private final static int DILATACION = 201;
    private final static int EROSION = 202;

    private final static int SEGMENTACION_GRADIENTE = 301;
    private final static int SEGMENTACION_ZONA_ROJA = 302;


    public enum Salida {
        ENTRADA, INTENSIDAD, OPERADOR_LOCAL, BINARIZACION,
        SEGMENTACION, RECONOCIMIENTO
    }

    //public enum TipoIntensidad {SIN_PROCESO, LUMINANCIA, AUMENTO_LINEAL_CONSTANTE, EQUALIZ_HISTOGRAMA, ZONAS_ROJAS, ZONAS_AZULES}
    public enum TipoIntensidad {
        SIN_PROCESO, LUMINANCIA, AUMENTO_LINEAL_CONSTRASTE, EQUALIZ_HISTOGRAMA, ZONAS_ROJAS, ZONAS_AZULES, ZONAS_VERDES
    }

    public enum TipoOperadorLocal {SIN_PROCESO, PASO_BAJO, PASO_ALTO, PASO_ALTO_GAUSSIANO, PASO_ALTO_POSITIVO, GRADIENTES, RESIDUO_DILATACION, RESIDUO_EROSION}

    public enum TipoBinarizacion {SIN_PROCESO, BINARIZACION_ROJA, BINARIZACION_OTSU, BINARIZACION_ADAPTATIVA}

    public enum TipoSegmentacion {SIN_PROCESO, SEGMENTACION, SEGMENTACION_ZONA_ROJA}

    public enum TipoReconocimiento {SIN_PROCESO}

    public Boolean getPantallaPartida() {
        return pantallaPartida;
    }

    public void setPantallaPartida(Boolean pantallaPartida) {
        this.pantallaPartida = pantallaPartida;
    }

    public Procesado() { //Constructor
        mostrarSalida = Salida.INTENSIDAD;
        tipoIntensidad = TipoIntensidad.LUMINANCIA;
        tipoOperadorLocal = TipoOperadorLocal.SIN_PROCESO;
        tipoBinarizacion = TipoBinarizacion.SIN_PROCESO;
        tipoSegmentacion = TipoSegmentacion.SIN_PROCESO;
        tipoReconocimiento = TipoReconocimiento.SIN_PROCESO;
        salidaintensidad = new Mat();
        salidatrlocal = new Mat();
        salidabinarizacion = new Mat();
        salidasegmentacion = new Mat();
        salidaocr = new Mat();
    }

    private void initHistograma() {
        canales = new MatOfInt(0);
        numero_bins = new MatOfInt(256);
        intervalo = new MatOfFloat(0, 256);
        hist = new Mat();
        imagenes = new ArrayList<Mat>();
        histograma = new float[256];
    }

    void mitadMitad(Mat entrada, Mat salida) {
        if (entrada.channels() > salida.channels())
            Imgproc.cvtColor(salida, salida, Imgproc.COLOR_GRAY2RGBA);
        if (entrada.channels() < salida.channels())
            Imgproc.cvtColor(entrada, entrada, Imgproc.COLOR_GRAY2RGBA);
//Representar la entrada en la mitad izquierda
        Rect mitad_izquierda = new Rect();
        mitad_izquierda.x = 0;
        mitad_izquierda.y = 0;
        mitad_izquierda.height = entrada.height();
        mitad_izquierda.width = entrada.width() / 2;
        Mat salida_mitad_izquierda = salida.submat(mitad_izquierda);
        Mat entrada_mitad_izquierda = entrada.submat(mitad_izquierda);
        entrada_mitad_izquierda.copyTo(salida_mitad_izquierda);
    }

    public Mat procesa(Mat entrada) {
        if (mostrarSalida == Salida.ENTRADA) {
            return entrada;
        }
        switch (tipoIntensidad) {
            case SIN_PROCESO:
                salidaintensidad = entrada;
                break;
            case LUMINANCIA:
                Imgproc.cvtColor(entrada, salidaintensidad, Imgproc.COLOR_RGBA2GRAY);
                break;
            case AUMENTO_LINEAL_CONSTRASTE:
                Mat gris = new Mat();
                Imgproc.cvtColor(entrada, gris, Imgproc.COLOR_RGBA2GRAY);
                aumentoLinealConstante(gris);
                gris.release();
                break;
            case EQUALIZ_HISTOGRAMA:
                Mat grey = new Mat();
                Imgproc.cvtColor(entrada, grey, Imgproc.COLOR_RGBA2GRAY);
                //Eq. Hist necesita gris
                Imgproc.equalizeHist(grey, salidaintensidad);
                grey.release();
                break;
            case ZONAS_ROJAS:
                //zonaRoja(entrada); //resultado en salidaintensidad
                soloUnColor(entrada, ROJO);
                break;
            case ZONAS_AZULES:
                //zonaAzul(entrada); //resultado en salidaintensidad
                soloUnColor(entrada, AZUL);
                break;
            case ZONAS_VERDES:
                //zonaVerde(entrada); //resultado en salidaintensidad
                soloUnColor(entrada, VERDE);
                break;
            default:
                salidaintensidad = entrada;
        }
        if (mostrarSalida == Salida.INTENSIDAD) {
            return salidaintensidad;
        }
        // Operador local
        switch (tipoOperadorLocal) {
            case SIN_PROCESO:
                salidatrlocal = salidaintensidad;
                break;
            case PASO_BAJO:
                pasoBajo(salidaintensidad); //resultado en salidatrlocal
                break;
            case PASO_ALTO:
                pasoAlto(salidaintensidad); //resultado en salidatrlocal
                break;
            case PASO_ALTO_GAUSSIANO:
                pasoAltoGaussiano(salidaintensidad);
                break;
            case PASO_ALTO_POSITIVO:
                pasoAltoPositivo(salidaintensidad); //resultado en salidatrlocal
                //return resPasoAltoNegativo;
                break;
            case GRADIENTES:
                gradiente(salidaintensidad);
                break;
            case RESIDUO_DILATACION:
                residuo(salidaintensidad, DILATACION);
                break;
            case RESIDUO_EROSION:
                residuo(salidaintensidad, EROSION);
                break;
        }
        if (mostrarSalida == Salida.OPERADOR_LOCAL) {
            return salidatrlocal;
        }
        // Binarización
        switch (tipoBinarizacion) {
            case SIN_PROCESO:
                salidabinarizacion = salidatrlocal;
                break;
            case BINARIZACION_ROJA:
                BinarizacionRoja(salidatrlocal);
                salidabinarizacion = salidatrlocal;
                break;
            case BINARIZACION_OTSU:
                BinarizacionOtsu(salidatrlocal);
                //salidabinarizacion = salidatrlocal;
                break;
            case BINARIZACION_ADAPTATIVA:
                BinarizacionAdaptativa(salidatrlocal);
                //salidabinarizacion = salidatrlocal;
                break;

        }
        if (mostrarSalida == Salida.BINARIZACION) {
            return salidabinarizacion;
        }
        // Segmentación
        switch (tipoSegmentacion) {
            case SIN_PROCESO:
                salidasegmentacion = salidabinarizacion;
                break;
            case SEGMENTACION:
                buscaCirculos(salidabinarizacion, SEGMENTACION_GRADIENTE);
                break;
            case SEGMENTACION_ZONA_ROJA:
                buscaCirculos(salidabinarizacion, SEGMENTACION_ZONA_ROJA);
                break;
        }

        if (mostrarSalida == Salida.SEGMENTACION) {
            return salidasegmentacion;
        }
// Reconocimiento OCR
        switch (tipoReconocimiento) {
            case SIN_PROCESO:
                salidaocr = salidabinarizacion;
                break;
        }
        return salidaocr;
        //Mat salida = entrada.clone();
        //return salida;
    }

    /**
     * SOLO COMPONENTE ROJO
     */
    void zonaRoja(Mat entrada) { //Ejemplo para ser rellenada en curso salidaintensidad = entrada;
        Mat red;
        Mat green;
        Mat blue;
        Mat maxGB;
        red = new Mat();
        green = new Mat();
        blue = new Mat();
        maxGB = new Mat();

        //Mat salida = new Mat();
        Core.extractChannel(entrada, red, 0);
        Core.extractChannel(entrada, green, 1);
        Core.extractChannel(entrada, blue, 2);
        Core.max(green, blue, maxGB);
        Core.subtract(red, maxGB, salidaintensidad);
        green.release();
        blue.release();
        red.release();
        maxGB.release();
    }

    void zonaAzul(Mat entrada) { //Ejemplo para ser rellenada en curso salidaintensidad = entrada;
        Mat red;
        Mat green;
        Mat blue;
        Mat maxRG;
        red = new Mat();
        green = new Mat();
        blue = new Mat();
        maxRG = new Mat();

        //Mat salida = new Mat();
        Core.extractChannel(entrada, red, 0);
        Core.extractChannel(entrada, green, 1);
        Core.extractChannel(entrada, blue, 2);
        Core.max(red, green, maxRG);
        Core.subtract(blue, maxRG, salidaintensidad);
        green.release();
        blue.release();
        red.release();
        maxRG.release();
    }


    void soloUnColor(Mat entrada, int color) {
        Mat red;
        Mat green;
        Mat blue;
        Mat maxOthers;
        red = new Mat();
        green = new Mat();
        blue = new Mat();
        maxOthers = new Mat();
        Core.extractChannel(entrada, red, 0);
        Core.extractChannel(entrada, green, 1);
        Core.extractChannel(entrada, blue, 2);
        switch (color) {
            case ROJO:
                Core.max(green, blue, maxOthers);
                Core.subtract(red, maxOthers, salidaintensidad);
                break;
            case VERDE:
                Core.max(red, blue, maxOthers);
                Core.subtract(green, maxOthers, salidaintensidad);
                break;
            case AZUL:
                Core.max(red, green, maxOthers);
                Core.subtract(blue, maxOthers, salidaintensidad);
                break;
        }
        green.release();
        blue.release();
        red.release();
        maxOthers.release();
    }

    /**
     * AUMENTO LINEAL CONTRASTE
     */
    void aumentoLinealConstante(Mat entrada) {
        //Ejemplo para ser rellenada en curso salidaintensidad = entrada;
        initHistograma();
        imagenes.clear(); //Eliminar imagen anterior si la hay
        imagenes.add(entrada); //Añadir imagen actual
        Imgproc.calcHist(imagenes, canales, new Mat(), hist,
                numero_bins, intervalo);
//Lectura del histograma a un array de float
        hist.get(0, 0, histograma);
//Calcular xmin y xmax
        int total_pixeles = entrada.cols() * entrada.rows();
        float porcentaje_saturacion = (float) 0.05;
        int pixeles_saturados = (int) (porcentaje_saturacion * total_pixeles);
        int xmin = 0;
        int xmax = 255;
        float acumulado = 0f;
        for (int n = 0; n < 256; n++) { //xmin
            acumulado = acumulado + histograma[n];
            if (acumulado > pixeles_saturados) {
                xmin = n;
                break;
            }
        }
        acumulado = 0;
        for (int n = 255; n >= 0; n--) { //xmax
            acumulado = acumulado + histograma[n];
            if (acumulado > pixeles_saturados) {
                xmax = n;
                break;
            }
        }
//Calculo de la salida
        Core.subtract(entrada, new Scalar(xmin), salidaintensidad);
        float pendiente = ((float) 255.0) / ((float) (xmax - xmin));
        Core.multiply(salidaintensidad, new Scalar(pendiente), salidaintensidad);
    }


    void pasoBajo(Mat entrada) {//Ejemplo para ser rellenada en curso salidatrlocal entrada;
        // Mat salida = new Mat();
        Mat pasoBajo = new Mat();
        int filter_size = 17;
        Size s = new Size(filter_size, filter_size);
// filtrado paso bajo, automático por medio de la función blur
        Imgproc.blur(entrada, pasoBajo, s);
// Hacer la resta. Los valores negativos saturan a cero
        //Core.subtract(pasoBajo, entrada, salidatrlocal);
//Aplicar Ganancia para ver mejor. La multiplicacion satura
        //Scalar ganancia = new Scalar(2);
        //Core.multiply(salidatrlocal, ganancia, salidatrlocal);
        salidatrlocal = pasoBajo.clone();
        pasoBajo.release();
        //return salidatrlocal;
    }

    void pasoAltoPositivo(Mat entrada) {//Ejemplo para ser rellenada en curso salidatrlocal entrada;
        // Mat salida = new Mat();
        Mat original_float = new Mat();
        entrada.convertTo(original_float, CvType.CV_32FC1);
        Mat paso_bajo_float = new Mat(entrada.width(), entrada.height(), CvType.CV_32FC1);
        Mat matrizRdo = new Mat(entrada.width(), entrada.height(), CvType.CV_32FC1);
        int filter_size = 17;
        Size s = new Size(filter_size, filter_size);
        Imgproc.blur(original_float, paso_bajo_float, s);
        Mat paso_alto_float = new Mat();
        Core.subtract(original_float, paso_bajo_float, paso_alto_float);
        Core.add(paso_alto_float, new Scalar(128), matrizRdo);
        matrizRdo.convertTo(salidatrlocal, CvType.CV_8UC1);
        //1º Convertir entrada a Flotante
        //2º Filtar paso bajo entrada
        //3º Restar entrada-paso bajo (flotante)
        //4º Sumar 128 a la resta, función add, con un escalar
        //5º convertir a Uchar8
        // ej pagina 28 de la unidad 2
        //Core.add();//sumar en 128
        // y luego convertir la matriz a unsignedchar
//Aplicar Ganancia para ver mejor. La multiplicacion satura
        //Scalar ganancia = new Scalar(2);
        //Core.multiply(resPaslAltoNegativo, ganancia, resPaslAltoNegativo);

    }

    void gradiente(Mat entrada) {
        Mat Gx = new Mat();
        Mat Gy = new Mat();
        //Derivada Horizontal
        Imgproc.Sobel(entrada, Gx, CvType.CV_32FC1, 1, 0); //Derivada primera respecto x
        //Derivada Vertical
        Imgproc.Sobel(entrada, Gy, CvType.CV_32FC1, 0, 1); //Derivada primera respecto y
        //Módulo del gradiente
        Mat Gx2 = new Mat();
        Mat Gy2 = new Mat();
        Core.multiply(Gx, Gx, Gx2); //Gx2 = Gx*Gx elemento a elemento
        Core.multiply(Gy, Gy, Gy2); //Gy2 = Gy*Gy elemento a elemento
        Mat ModGrad2 = new Mat();
        Core.add(Gx2, Gy2, ModGrad2);
        Mat ModGrad = new Mat();
        Core.sqrt(ModGrad2, ModGrad);
        //COnvertir a unsigned char
        ModGrad.convertTo(salidatrlocal, CvType.CV_8UC1);
        ModGrad.release();
        ModGrad2.release();
        Gy2.release();
        Gx2.release();
        Gy.release();
        Gx.release();
    }

    void pasoAlto(Mat entrada) {//Ejemplo para ser rellenada en curso salidatrlocal entrada;
        // Mat salida = new Mat();
        Mat pasoAlto = new Mat();
        int filter_size = 17;
        Size s = new Size(filter_size, filter_size);
// filtrado paso bajo, automático por medio de la función blur
        Imgproc.blur(entrada, pasoAlto, s);
// Hacer la resta. Los valores negativos saturan a cero
        Core.subtract(pasoAlto, entrada, salidatrlocal);
//Aplicar Ganancia para ver mejor. La multiplicacion satura
        Scalar ganancia = new Scalar(2);
        Core.multiply(salidatrlocal, ganancia, salidatrlocal);
        pasoAlto.release();
    }

    void pasoAltoGaussiano(Mat entrada) {//Ejemplo para ser rellenada en curso salidatrlocal entrada;
        // Mat salida = new Mat();
        Mat pasoAlto = new Mat();
        int filter_size = 17;
        double sigma = filter_size / 3.5;
        double kSize = Math.rint(sigma * 6);
        if (kSize % 2 == 0)
            kSize++;

        Size s = new Size(kSize, kSize);
        //promediado filter_size/3.5 y lo multiplico por 6 = 29.14
        Imgproc.GaussianBlur(entrada, pasoAlto, s, sigma, sigma);
// Hacer la resta. Los valores negativos saturan a cero
        Core.subtract(pasoAlto, entrada, salidatrlocal);
//Aplicar Ganancia para ver mejor. La multiplicacion satura
        Scalar ganancia = new Scalar(2);
        Core.multiply(salidatrlocal, ganancia, salidatrlocal);
        pasoAlto.release();
    }

    void residuo(Mat entrada, int tipo) {
        double tam = 3;
        Mat gray = new Mat();
        Mat gray_dilation = new Mat(); // Result
        Mat gray_erosion = new Mat(); // Result

        if (entrada.channels() > 1) // doy supuesto que es color....
        {
            Imgproc.cvtColor(entrada, gray, Imgproc.COLOR_RGBA2GRAY); //Me aseguro que la imagen que llega es gris
        }
        Mat SE = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(tam, tam));
        Imgproc.dilate(gray, gray_dilation, SE); // 3x3 dilation
        Imgproc.erode(gray, gray_erosion, SE); // 3x3 erosion


        if (tipo == DILATACION) {
            Core.subtract(gray_dilation, gray, salidatrlocal);
        } else {
            Core.subtract(gray, gray_erosion, salidatrlocal);
        }
        SE.release();
        gray_erosion.release();
        gray_dilation.release();
        gray.release();
    }


    public void BinarizacionRoja(Mat salidatrlocal) {
        // soloUnColor(salidatrlocal, ROJO);
        Core.MinMaxLocResult minMax = Core.minMaxLoc(salidatrlocal);
        int maximum = (int) minMax.maxVal;
        int thresh = maximum / 4;
        Imgproc.threshold(salidatrlocal, salidatrlocal, thresh, 255, Imgproc.THRESH_BINARY);
        //salidatrlocal = salidaintensidad.clone();
    }

    public void BinarizacionOtsu(Mat salidatrlocal) {
        Imgproc.threshold(salidatrlocal, salidabinarizacion, 0, 255, Imgproc.THRESH_OTSU |
                Imgproc.THRESH_BINARY);
    }

    public void BinarizacionAdaptativa(Mat salidatrlocal) {
        int contraste = 18;
        int tamano = 5;
        Imgproc.adaptiveThreshold(salidatrlocal, salidabinarizacion, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C,
                Imgproc.THRESH_BINARY, tamano, -contraste);

    }


    public void buscaCirculos(Mat salidabinarizacion, int tipoSegementacion) {
        List<MatOfPoint> blobs = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        salidasegmentacion = salidabinarizacion.clone();//Copia porque finContours modifica entrada
        Imgproc.cvtColor(salidasegmentacion, salidasegmentacion, Imgproc.COLOR_GRAY2RGBA);
        Imgproc.findContours(salidabinarizacion, blobs, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE);
        //ImgProc.RETR_TREE:  RETR_CCOMP
        // Imgproc.findContours(salidabinarizacion, blobs, hierarchy, Imgproc.RETR_TREE,  Imgproc.CHAIN_APPROX_NONE );
        int minimumHeight = 30;
        float maxratio = (float) 0.75;
// Seleccionar candidatos a circulos
        getProfundidad(hierarchy);
        for (int c = 0; c < blobs.size(); c++) {
            double[] data = hierarchy.get(0, c);
            int parent = (int) data[3];
            if (parent < 0) //Contorno exterior: rechazar
                continue;
            Rect BB = Imgproc.boundingRect(blobs.get(c));


// Comprobar tamaño
            if (BB.width < minimumHeight || BB.height < minimumHeight)
                continue;
// Comprobar anchura similar a altura
            float wf = BB.width;
            float hf = BB.height;
            float ratio = wf / hf;
            if (ratio < maxratio || ratio > 1.0 / maxratio)
                continue;
// Comprobar no está cerca del borde
            if (BB.x < 2 || BB.y < 2)
                continue;
            if (salidabinarizacion.width() - (BB.x + BB.width) < 3 || salidabinarizacion.height() - (BB.y + BB.height) < 3)
                continue;
// Aqui cumple todos los criterios. Dibujamos
            Log.wtf("* BUSCA CIRCULOS *", "Pasamos criba, empieza test circularidad");
            //TEST DE CIRCULARIDAD, en transparencias código obtener centro
            Point center = getCenterPoint(blobs.get(c));
            Log.wtf("* BUSCA CIRCULOS *", "El punto central es .... " + center);
            //Ahora falta por cada punto del contorno calcula distancia al centro, guardando min y Max y devuelvo cociente si
            // próximas a 1 coseno de 20 grados es el máximo del límite
            Boolean bCirculo = isCirculo(blobs.get(c), center, tipoSegementacion);
            if (bCirculo) {
                Log.wtf("* BUSCA CIRCULOS *", "Estamos dentro de un círculo");
                Integer[] depth = profundidad.get(c);
                if (depth[4] % 2 == 0) {
                    //Log.wtf("* BUSCA CIRCULOS *", "elemento que muestro ....");
                    //Log.wtf("* BUSCA CIRCULOS *", "c => " + c + "; hijo => " + depth[2] + "; padre => " + depth[3] + "; profundidad => " + depth[4]);
                    //Log.wtf("* BUSCA CIRCULOS *", "El abuelo es ... " + profundidad.get(depth[3])[3]);
                    final Point P1 = new Point(BB.x, BB.y);
                    final Point P2 = new Point(BB.x + BB.width - 1, BB.y + BB.height - 1);
                    if (tipoSegementacion == SEGMENTACION_ZONA_ROJA) {
                        paintRectangle(P1, P2, tipoSegementacion);
                    } else {
                        if (profundidad.get(depth[3])[3] != -1) {
                            Log.wtf("* BUSCA CIRCULOS *", "Tiene abuelo, y es ... " + profundidad.get(depth[3])[3]);
                            paintRectangle(P1, P2, tipoSegementacion);
                     /*final Point P1 = new Point(BB.x, BB.y);
                     final Point P2 = new Point(BB.x + BB.width-1, BB.y + BB.height-1);
                     //Imgproc.rectangle(salidasegmentacion, P1, P2, new Scalar(255, 0, 0));
                     Imgproc.rectangle(salidasegmentacion, P1, P2, new Scalar(0, 0, 255));*/
                        }
                    }
                }
            }
        } // for
    }//de la function

    private void paintRectangle(Point P1, Point P2, int tipo) {
        Scalar color;
        if (tipo == SEGMENTACION_GRADIENTE) {
            color = new Scalar(255, 0, 0);
        } else {
            color = new Scalar(0, 0, 255);
        }
        Imgproc.rectangle(salidasegmentacion, P1, P2, color);
    }


    private Point getCenterPoint(MatOfPoint curBlob) {
        Point Sum = new Point(0.0, 0.0);
        Size sizeA = curBlob.size();
        for (int x = 0; x < sizeA.height; x++)
            for (int y = 0; y < sizeA.width; y++) {
                double[] pp = curBlob.get(x, y);
                Sum.x += pp[0];
                Sum.y += pp[1];
            }
        double number_countour_points = sizeA.width * sizeA.height;
        Sum.x /= number_countour_points;
        Sum.y /= number_countour_points;
        return Sum;
    }


    // *** test de circularidad
    private Boolean isCirculo(MatOfPoint curBlob, Point center, int tipo) {
        Boolean lRes;
        double minDistance = 0;
        double MaxDistance = 0;
        final double xCenter = center.x;
        final double yCenter = center.y;
        Size sizeA = curBlob.size();
        for (int x = 0; x < sizeA.height; x++)
            for (int y = 0; y < sizeA.width; y++) {
                double[] point = curBlob.get(x, y);
                double dist = Math.hypot(xCenter - point[0], yCenter - point[1]);
                if (x == 0 && y == 0) {
                    minDistance = Math.hypot(xCenter - point[0], yCenter - point[1]);
                    MaxDistance = Math.hypot(xCenter - point[0], yCenter - point[1]);
                }

                if (dist < minDistance) {
                    minDistance = dist;
                }
                if (dist > MaxDistance) {
                    MaxDistance = dist;
                }
            }
        Log.wtf("* BUSCA CIRCULOS *", "Dentro de isCirculo, MaxDistance => " + MaxDistance + "; minDistnace => " + minDistance);
        double rdo = minDistance / MaxDistance;
        //double rdoRadianes = (rdo * Math.PI) / 180.0;
        lRes = (rdo >= Math.sin(tipo == SEGMENTACION_GRADIENTE ? 20 : 21));
        //lRes = (rdo >= Math.cos(20));
        Log.wtf("* BUSCA CIRCULOS *", "Dentro de isCirculo, división ... " + rdo);
        Log.wtf("* BUSCA CIRCULOS *", "Dentro de isCirculo, seno de 20 ... " + Math.sin(20));
        Log.wtf("* BUSCA CIRCULOS *", "Dentro de isCirculo, y es ... " + lRes);
        return lRes;
    }

    // *** Obtengo la profundidad y toda al jerarquía Padre, hermanos e hijos
    private void getProfundidad(Mat hierachy) {
        if (profundidad != null) {
            if (profundidad.size() > 0)
                profundidad.clear();
        }
        profundidad = new ArrayList<Integer[]>();
        for (int i = 0; i < hierachy.cols(); i++) {
            Integer[] med = new Integer[5];
            med[0] = 0;
            med[1] = 0;
            med[2] = 0;
            med[3] = 0;
            med[4] = 0;
            profundidad.add(med);
        }
        for (int i = 0; i < hierachy.cols(); i++) {
            int next = (int) hierachy.get(0, i)[0];
            int prev = (int) hierachy.get(0, i)[1];
            int first_child = (int) hierachy.get(0, i)[2];
            int parent = (int) hierachy.get(0, i)[3];
            if (profundidad.get(i)[4] > 0)
                continue;
            if (parent < 0) { // si no tiene padre ....
                Integer[] chg = new Integer[5];
                chg[0] = next;
                chg[1] = prev;
                chg[2] = first_child;
                chg[3] = parent;
                // la profundidad es 1 ....
                chg[4] = 1;
                profundidad.set(i, chg);
            } // si tiene padre,
            else if (profundidad.get(parent)[4] > 0) {
                Integer[] chg = new Integer[5];
                chg[0] = next;
                chg[1] = prev;
                chg[2] = first_child;
                chg[3] = parent;
                //recupero la profundidad del padre y le sumo 1 ...(si padre tiene profundiad = 1, yo, (hijo) profundidad 2)
                chg[4] = profundidad.get(parent)[4] + 1;
                profundidad.set(i, chg);
            }
        }

    }

    public Mat getSalidaintensidad() {
        return salidaintensidad;
    }

    public void setSalidaintensidad(Mat salidaintensidad) {
        this.salidaintensidad = salidaintensidad;
    }

    public Mat getSalidatrlocal() {
        return salidatrlocal;
    }

    public void setSalidatrlocal(Mat salidatrlocal) {
        this.salidatrlocal = salidatrlocal;
    }

    public Mat getSalidabinarizacion() {
        return salidabinarizacion;
    }

    public void setSalidabinarizacion(Mat salidabinarizacion) {
        this.salidabinarizacion = salidabinarizacion;
    }

    public Mat getSalidasegmentacion() {
        return salidasegmentacion;
    }

    public void setSalidasegmentacion(Mat salidasegmentacion) {
        this.salidasegmentacion = salidasegmentacion;
    }

    public Mat getSalidaocr() {
        return salidaocr;
    }

    public void setSalidaocr(Mat salidaocr) {
        this.salidaocr = salidaocr;
    }

    public Salida getMostrarSalida() {
        return mostrarSalida;
    }

    public void setMostrarSalida(Salida mostrarSalida) {
        this.mostrarSalida = mostrarSalida;
    }

    public TipoIntensidad getTipoIntensidad() {
        return tipoIntensidad;
    }

    public void setTipoIntensidad(TipoIntensidad tipoIntensidad) {
        this.tipoIntensidad = tipoIntensidad;
    }

    public TipoOperadorLocal getTipoOperadorLocal() {
        return tipoOperadorLocal;
    }

    public void setTipoOperadorLocal(TipoOperadorLocal tipoOperadorLocal) {
        this.tipoOperadorLocal = tipoOperadorLocal;
    }

    public TipoBinarizacion getTipoBinarizacion() {
        return tipoBinarizacion;
    }

    public void setTipoBinarizacion(TipoBinarizacion tipoBinarizacion) {
        this.tipoBinarizacion = tipoBinarizacion;
    }

    public TipoSegmentacion getTipoSegmentacion() {
        return tipoSegmentacion;
    }

    public void setTipoSegmentacion(TipoSegmentacion tipoSegmentacion) {
        this.tipoSegmentacion = tipoSegmentacion;
    }

    public TipoReconocimiento getTipoReconocimiento() {
        return tipoReconocimiento;
    }

    public void setTipoReconocimiento(TipoReconocimiento tipoReconocimiento) {
        this.tipoReconocimiento = tipoReconocimiento;
    }
}
